import { ContactService } from '@modules/contact/services/contact.service';
import { UserService } from '@modules/user/services/user.service';
import { INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const appContext: INestApplicationContext =
    await NestFactory.createApplicationContext(AppModule);

  const userSeeder = appContext.get(UserService);
  const contactSeeder = appContext.get(ContactService);

  try {
    await contactSeeder.seed();
    await userSeeder.seed();
  } finally {
    appContext.close();
  }
}

bootstrap()
  .then(() => console.log('seeding done'))
  .catch((e) => console.log('seeding failed', e));

import { IContact } from '../contact.entity';

export const contacts: IContact[] = [
  {
    id: 1,
    ext: '+91',
    number: '9582348271',
    name: 'test1',
  },
  {
    id: 2,
    ext: '+91',
    number: '9582348273',
    name: 'test2',
  },
];

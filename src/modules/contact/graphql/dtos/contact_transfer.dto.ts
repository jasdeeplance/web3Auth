import { Field, ArgsType, Int } from '@nestjs/graphql';

@ArgsType()
export class ContactTransferArgs {
  @Field(() => Int)
  receiver: number;

  @Field(() => Int)
  id: number;
}

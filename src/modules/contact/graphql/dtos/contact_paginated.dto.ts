import { ArgsType, Field, Int } from '@nestjs/graphql';

@ArgsType()
export class ContactPaginatedArgs {
  @Field(() => Int)
  offset: number;

  @Field(() => Int)
  size: number;
}

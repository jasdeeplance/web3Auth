import { Args, Query, Resolver } from '@nestjs/graphql';
import { ContactService } from '../services/contact.service';
import { ContactModel } from './models/contact.model';
import { ContactPaginatedModel } from './models/contact_paginated.model';
import { ContactPaginatedArgs } from './dtos/contact_paginated.dto';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '@modules/auth/guards/auth.jwt-guard';
import { RolesGuard } from '@modules/auth/guards/auth.role-guard';
import { Role } from '@modules/user/types';
import { Roles } from '@modules/auth/decorators/auth.roles-decorator';
import { AuthInfo } from '@modules/auth/types';
import { CurrentUser } from '@modules/auth/decorators/auth.user-decorator';

@Resolver(() => ContactModel)
export class ContactResolver {
  constructor(private contactService: ContactService) {}

  @UseGuards(GqlAuthGuard, RolesGuard)
  @Roles([Role.User, Role.Admin])
  @Query(() => ContactPaginatedModel)
  async contacts(
    @CurrentUser() info: AuthInfo,
    @Args() args: ContactPaginatedArgs,
  ): Promise<ContactPaginatedModel> {
    const { offset, size } = args;
    const { userId } = info;
    const paginatedContacts = await this.contactService.getAll(
      userId,
      size,
      offset,
    );
    return paginatedContacts;
  }
}

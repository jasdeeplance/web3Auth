import { Field, Int, ObjectType } from '@nestjs/graphql';
import { ContactModel } from './contact.model';

@ObjectType()
export class ContactPaginatedModel {
  @Field(() => [ContactModel])
  edges: ContactModel[];

  @Field(() => Int)
  totalCount: number;
}

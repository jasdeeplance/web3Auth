import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ContactModel {
  @Field()
  name: string;

  @Field()
  ext: string;

  @Field()
  number: string;
}

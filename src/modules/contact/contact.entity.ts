import { IUser, User } from '@modules/user/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

export interface IContact {
  id: number;
  name: string;
  ext: string;
  number: string;
  user?: IUser;
}

@Entity()
export class Contact implements IContact {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  ext: string;

  @Column({ unique: true })
  number: string;

  @ManyToOne(() => User, (user) => user.contacts, {
    nullable: true,
    eager: true,
  })
  user?: IUser;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

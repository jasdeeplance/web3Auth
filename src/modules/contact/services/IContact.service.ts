import { ContactPaginatedModel } from '../graphql/models/contact_paginated.model';
import { IContact } from '../contact.entity';

export interface IContactService {
  getAll(
    owner: number,
    take: number,
    skip: number,
  ): Promise<ContactPaginatedModel>;
  getContactById(id: number): Promise<IContact>;
}

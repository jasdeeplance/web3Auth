import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IContact, Contact } from '@modules/contact/contact.entity';
import { ContactPaginatedModel } from '../graphql/models/contact_paginated.model';
import { IContactService } from './IContact.service';
import { contacts } from '../seeder/contact.data';
import { UserService } from '@modules/user/services/user.service';

@Injectable()
export class ContactService implements IContactService {
  private readonly logger = new Logger(UserService.name);

  constructor(
    private userService: UserService,
    @InjectRepository(Contact)
    private contactRepository: Repository<Contact>,
  ) {}

  async getAll(
    userId: number,
    take: number,
    skip: number,
  ): Promise<ContactPaginatedModel> {
    this.logger.log(`fetching contacts for user: ${userId} at offset ${skip}`);
    const [edges, totalCount] = await this.contactRepository.findAndCount({
      where: { user: { id: userId } },
      take,
      skip,
    });
    return {
      edges,
      totalCount,
    };
  }

  async getContactById(id: number): Promise<Contact> {
    const contact = await this.contactRepository.findOneBy({ id });
    return contact;
  }

  async seed(): Promise<void> {
    const promises: Array<Promise<IContact>> = [];
    contacts.map((contact: IContact) => {
      promises.push(this.contactRepository.save(contact));
    });
    await Promise.all(promises);
  }

  private validateOwnership(contact: IContact, userId: number): boolean {
    if (!contact.user) {
      return false;
    }
    return contact.user!.id === userId;
  }
}

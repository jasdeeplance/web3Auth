import { UserModule } from '@modules/user/user.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContactResolver } from './graphql/contact.resolver';
import { Contact } from './contact.entity';
import { ContactService } from './services/contact.service';

@Module({
  imports: [TypeOrmModule.forFeature([Contact]), UserModule],
  providers: [ContactService, ContactResolver],
})
export class ContactModule {}

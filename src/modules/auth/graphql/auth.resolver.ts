import { NotFoundException } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { AuthService } from '../services/auth.service';
import { UserSigninArgs } from './dtos/user_signin.dto';
import { AuthModel } from './models/auth.model';

@Resolver(() => AuthModel)
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => AuthModel)
  async signIn(@Args() args: UserSigninArgs): Promise<AuthModel> {
    const { email, password } = args;
    const user = await this.authService.validateUser(email, password);
    if (!user) {
      throw new NotFoundException();
    }
    const token = this.authService.createSession(user);
    return { token };
  }
}

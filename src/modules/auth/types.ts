import { Role } from '@modules/user/types';

export type AuthInfo = {
  userId: number;
  roles: Role[];
};

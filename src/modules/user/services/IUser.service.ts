import { IUser } from '../user.entity';

export interface IUserService {
  findUserByEmail(email: string): Promise<IUser | null>;
  getUserById(id: number): Promise<IUser>;
}

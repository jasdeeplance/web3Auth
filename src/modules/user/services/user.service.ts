import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IUser, User } from '@modules/user/user.entity';
import { IUserService } from './IUser.service';
import { users } from '../seeder/user.data';

@Injectable()
export class UserService implements IUserService {
  private readonly logger = new Logger(UserService.name);

  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findUserByEmail(email: string): Promise<IUser | null> {
    const user = await this.usersRepository.findOneBy({ email });
    return user;
  }

  async getUserById(id: number): Promise<IUser> {
    this.logger.log(`fetching user with ${id}`);
    return this.usersRepository.findOneBy({ id });
  }

  async seed(): Promise<void> {
    const promises: Array<Promise<IUser>> = [];
    users.map((user: IUser) => {
      promises.push(this.usersRepository.save(user));
    });
    await Promise.all(promises);
    this.logger.log(`seeded ${promises.length} users`);
  }
}

import { IUser } from '@modules/user/user.entity';

export interface IContext {
  user: IUser;
}

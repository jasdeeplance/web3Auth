import { DatabaseType, DataSource } from 'typeorm';
import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { MySQLConfigModule } from '@config/mysql/mysql.module';
import { MysqlConfigService } from '@config/mysql/mysql.service';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [MySQLConfigModule],
      useFactory: async (mysqlConfigService: MysqlConfigService) => ({
        type: 'mysql' as DatabaseType,
        host: mysqlConfigService.host,
        port: mysqlConfigService.port,
        username: mysqlConfigService.username,
        password: mysqlConfigService.password,
        database: mysqlConfigService.database,
        autoLoadEntities: true,
        synchronize: true,
      }),
      inject: [MysqlConfigService],
      dataSourceFactory: async (options) => {
        const dataSource = await new DataSource(options).initialize();
        return dataSource;
      },
    } as TypeOrmModuleAsyncOptions),
  ],
})
export class MysqlProviderModule {}

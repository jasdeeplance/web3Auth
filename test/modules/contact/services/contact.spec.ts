import { ContactModel } from '@modules/contact/graphql/models/contact.model';
import { ContactPaginatedModel } from '@modules/contact/graphql/models/contact_paginated.model';
import { IContact, Contact } from '@modules/contact/contact.entity';
import { ContactModule } from '@modules/contact/contact.module';
import { contacts } from '@modules/contact/seeder/contact.data';
import { IContactService } from '@modules/contact/services/IContact.service';
import { ContactService } from '@modules/contact/services/contact.service';
import { users } from '@modules/user/seeder/user.data';
import { IUser, User } from '@modules/user/user.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { loadFixtures, TypeOrmMysqlTestingModule } from '@test/helper';
import { DataSource } from 'typeorm';

describe('ContactService', () => {
  let service: IContactService;
  let moduleRef: TestingModule;
  let dataSource: DataSource;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [...TypeOrmMysqlTestingModule(), ContactModule],
    }).compile();

    service = moduleRef.get<IContactService>(ContactService);
    dataSource = moduleRef.get<DataSource>(DataSource);
    await loadFixtures<IContact>(dataSource, contacts, Contact);
    await loadFixtures<IUser>(dataSource, users, User);
  });

  afterAll(async () => {
    await moduleRef.close();
  });

  it('can get contact by id', async () => {
    const contact = await service.getContactById(1);
    expect(contact.id).toBe(1);
  });

  it('can get paginated contacts', async () => {
    // get 1 contact for 0 offset for user id 1
    const res: ContactPaginatedModel = await service.getAll(1, 1, 0);
    const { edges: contacts } = res;
    const sanitizedContacts: Array<ContactModel> = contacts.map(
      (contact: Contact) => {
        delete contact.created_at;
        delete contact.updated_at;
        return contact;
      },
    );
    expect(sanitizedContacts).toMatchSnapshot();
  });
});
